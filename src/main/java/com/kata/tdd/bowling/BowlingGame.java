package com.kata.tdd.bowling;

import java.util.ArrayList;
import java.util.List;

class BowlingGame {
    private Frame frame;
    private List<Frame> frames = new ArrayList<>();

    BowlingGame() {
        frame = new Frame();
        frames.add(frame);
    }

    void addRoll(String roll) {
        if(frame.isFull()) {
            frame = new Frame();
            frames.add(frame);
        }

        frame.addRoll(roll);
    }

    String getTotalScore() {
        int totalScore = 0;
        for (int frameIndex = 0; frameIndex < frames.size(); frameIndex++) {
            Frame currentFrame = frames.get(frameIndex);
            totalScore += currentFrame.getFrameScore();
            totalScore += getSpareBonus(frameIndex, currentFrame);
            totalScore += getStrikeBonus(frameIndex, currentFrame);
        }
        return String.valueOf(totalScore);
    }

    private int getStrikeBonus(int frameIndex, Frame currentFrame) {
        int strikeBonus = 0;
        if(currentFrame.isStrike()) {
            Frame nextFrame = frames.get(frameIndex + 1);
            strikeBonus += nextFrame.getFrameScore();

            if(nextFrame.isStrike()) {
                strikeBonus += frames.get(frameIndex + 2).getFirstRoll();
            }

        }
        return strikeBonus;
    }

    private int getSpareBonus(int frameIndex, Frame currentFrame) {
        int spareBonus = 0;

        if (currentFrame.isSpare()) {
            spareBonus = frames.get(frameIndex+1).getFirstRoll();
        }
        return spareBonus;
    }
}
