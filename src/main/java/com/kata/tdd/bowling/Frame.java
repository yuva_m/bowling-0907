package com.kata.tdd.bowling;

class Frame {
    private int[] rolls;
    private int rollIndex;

    Frame() {
        rolls = new int[2];
        rollIndex = 0;
    }

    void addRoll(String roll) {
        if ("/".equals(roll)) {
            rolls[rollIndex] = 10 - rolls[0];
        } else if ("X".equals(roll)) {
            rolls[rollIndex] = 10;
            rollIndex++;
        } else if ("-".equals(roll)) {
            rolls[rollIndex] = 0;
        } else {
            rolls[rollIndex] = Integer.valueOf(roll);
        }
        rollIndex++;
    }

    int getFrameScore() {
        return rolls[0] + rolls[1];
    }

    boolean isFull() {
        return rollIndex > 1;
    }

    boolean isStrike() {
        return rolls[0] == 10;
    }

    boolean isSpare() {
        return getFrameScore() == 10 && rolls[0] != 10;
    }

    int getFirstRoll() {
        return rolls[0];
    }
}