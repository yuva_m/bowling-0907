package com.kata.tdd.bowling;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class BowlingGameTest {

    private BowlingGame bowlingGame;

    @Before
    public void init() {
        bowlingGame = new BowlingGame();
    }

    @Test
    public void totalScoreShouldBe0WhenGameBegins() {
        BowlingGame bowlingGame = new BowlingGame();
        assertThat(bowlingGame.getTotalScore(), is("0"));
    }

    @Test
    public void totalScoreShouldBe5ForARollOf5() {
        bowlingGame.addRoll("5");
        assertThat(bowlingGame.getTotalScore(), is("5"));
    }

    @Test
    public void totalScoreShouldBe8ForTwoRollsOf5And3() {
        bowlingGame.addRoll("5");
        bowlingGame.addRoll("3");
        assertThat(bowlingGame.getTotalScore(), is("8"));
    }

    @Test
    public void totalScoreShouldBe18ForFourRollsOf_4_5_3_6() {
        bowlingGame.addRoll("4");
        bowlingGame.addRoll("5");
        bowlingGame.addRoll("3");
        bowlingGame.addRoll("6");
        assertThat(bowlingGame.getTotalScore(), is("18"));
    }

    @Test
    public void totalScoreShouldBe22ForASpareFollowedByRollsOf3And6() {
        bowlingGame.addRoll("4");
        bowlingGame.addRoll("/");
        bowlingGame.addRoll("3");
        bowlingGame.addRoll("6");
        assertThat(bowlingGame.getTotalScore(), is("22"));
    }

    @Test
    public void totalScoreShouldBe28ForAStrikeFollowedByRollsOf3And6() {
        bowlingGame.addRoll("X");
        bowlingGame.addRoll("3");
        bowlingGame.addRoll("6");
        assertThat(bowlingGame.getTotalScore(), is("28"));
    }

    @Test
    public void totalScoreShouldBe51ForTwoConsecutiveStrikesFollowedByRollsOf3And6() {
        bowlingGame.addRoll("X");
        bowlingGame.addRoll("X");
        bowlingGame.addRoll("3");
        bowlingGame.addRoll("6");
        assertThat(bowlingGame.getTotalScore(), is("51"));
    }

    @Test
    public void totalScoreShouldBe124ForRollsOfGivenRandomValuesWithStrikeAndSpare() {
        playAGameForTotalScoreOf124();

        assertThat(bowlingGame.getTotalScore(), is("124"));
    }

    private void playAGameForTotalScoreOf124() {
        bowlingGame.addRoll("X");

        bowlingGame.addRoll("X");

        bowlingGame.addRoll("3");
        bowlingGame.addRoll("6");

        bowlingGame.addRoll("1");
        bowlingGame.addRoll("/");

        bowlingGame.addRoll("4");
        bowlingGame.addRoll("3");

        bowlingGame.addRoll("2");
        bowlingGame.addRoll("6");

        bowlingGame.addRoll("2");
        bowlingGame.addRoll("3");

        bowlingGame.addRoll("1");
        bowlingGame.addRoll("4");

        bowlingGame.addRoll("6");
        bowlingGame.addRoll("1");

        bowlingGame.addRoll("7");
        bowlingGame.addRoll("/");

        bowlingGame.addRoll("8");
        bowlingGame.addRoll("1");
    }
}
