package com.kata.tdd.bowling;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class FrameTest {

    private Frame frame;

    @Before
    public void init() {
        frame = new Frame();
    }

    @Test
    public void frameScoreShouldBe0WhenGameBegins() {
        assertThat(frame.getFrameScore(), is(0));
    }

    @Test
    public void frameScoreShouldBe5ForARollOf5() {
        frame.addRoll("5");
        assertThat(frame.getFrameScore(), is(5));
    }

    @Test
    public void frameScoreShouldBe8ForTwoRollsOf5And3() {
        frame.addRoll("5");
        frame.addRoll("3");
        assertThat(frame.getFrameScore(), is(8));
    }

    @Test
    public void frameScoreShouldBe10ForASpare() {
        frame.addRoll("4");
        frame.addRoll("/");
        assertThat(frame.getFrameScore(), is(10));
    }

    @Test
    public void frameScoreShouldBe10ForAStrike() {
        frame.addRoll("X");
        assertThat(frame.getFrameScore(), is(10));
    }

    @Test
    public void frameScoreShouldBe3ForRollsOf4AndAMiss() {
        frame.addRoll("3");
        frame.addRoll("-");
        assertThat(frame.getFrameScore(), is(3));
    }

    @Test
    public void isStrikeShouldReturnTrueForAStrikeFrame() {
        frame.addRoll("X");
        assertTrue(frame.isStrike());
    }

    @Test
    public void isSpareShouldReturnTrueForRollsOf4And6() {
        frame.addRoll("4");
        frame.addRoll("6");
        assertTrue(frame.isSpare());
    }

    @Test
    public void getFirstRollShouldReturnValueOfRollAtIndex0() {
        frame.addRoll("4");
        frame.addRoll("6");
        assertThat(frame.getFirstRoll(), is(4));
    }

    @Test
    public void isFullShouldReturnTrueAfterASingleRollOfX() {
        frame.addRoll("X");
        assertTrue(frame.isFull());
    }
}
