# Bowling Game - Score Calculator
> This application will produce the total score of a bowling game for a given valid sequence of rolls for one line.

### Game Rule(s):
- Each game, or â€œlineâ€ of bowling, includes ten turns, or â€œframesâ€ for the bowler.
- In each frame, the bowler gets up to two tries to knock down all the pins.
- If in two tries, he fails to knock them all down, his score for that frame is the total number of pins knocked down in his two tries.
- If in two tries he knocks them all down, this is called a â€œspareâ€ and his score for the frame is ten plus the number of pins knocked down on his next throw (in his next turn).
- If on his first try in the frame he knocks down all the pins, this is called a â€œstrikeâ€. His turn is over, and his score for the frame is ten plus the simple total of the pins knocked down in his next two rolls.
- If he gets a spare or strike in the last (tenth) frame, the bowler gets to throw one or two more bonus balls, respectively. These bonus throws are taken as part of the same turn. If the bonus throws knock down all the pins, the process does not repeat: the bonus throws are only used to calculate the score of the final frame.
- The game score is the total of all frame scores.

#### More information available at:
- https://github.com/stephane-genicot/katas/blob/master/Bowling.md
- http://www.topendsports.com/sport/tenpin/scoring.htm
- http://codingdojo.org/kata/Bowling/ 

### Note:
- Roll(s) are not validated
- Intermediate total score is not provided

### Prerequisites:
- Java 1.8+
- Maven 3.5+

### How to run/test:
- Perform maven clean install  
	`mvn clean install -U`
- Start server  
	`mvn spring-boot:run`
- Now server is ready to accept request(s)
    - Request should contain a json string array of score(s) like:
		```
		{
			"rolls" : ["1", "6", "3", "/", "6", "4", "2", "7", "6", "2",
			 "5", "4", "5", "4", "5", "3", "X", "1", "6"]
		}
		```

	- Response will have the final score (including bonus score) like:
		```
		{
			"score" : "96"
		}
		```

	**URL** : [http://localhost:8080/game/bowling/score](http://localhost:8080/game/bowling/score)  
	**HTTP method**: ***POST***

	**Valid input for a good roll:**
	- "1", "2",..."9"
	- "0" or "-" for a miss
	- "X" or "10" for a strike
	- "/" for a spare (this can be indicated by (10-first roll) of the frame as well)